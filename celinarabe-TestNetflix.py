#!/usr/bin/env python3

# -------
# imports
# -------

#thing I DID NOT create tests for: create_cache

from Netflix import netflix_eval,create_cache
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):
    # ----
    # eval1:
    # ----

    def test_eval_returns_correct_predictions_and_RMSE_1(self):
        r = StringIO("1:\n30878\n2647871\n1772839\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.7\n3.5\n3.9\n0.69\n")

    def test_eval_returns_correct_predictions_and_RMSE_2(self):
        r = StringIO("15010:\n624017\n776655\n113549\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "15010:\n3.1\n3.5\n3.2\n0.32\n")

    def test_eval_returns_correct_predictions_and_RMSE_3(self):
        r = StringIO("15017:\n1166323\n1071230\n382960\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "15017:\n3.0\n3.7\n3.2\n0.73\n")

    def test_eval_returns_correct_predictions_and_RMSE_4(self):
        r = StringIO("15909:\n1414778\n1053860\n1323080\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "15909:\n3.6\n3.4\n3.3\n0.35\n")

    def test_eval_returns_correct_predictions_and_RMSE_5(self):
        r = StringIO("2180:\n2568963\n1953215\n181699\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "2180:\n3.1\n3.2\n3.1\n0.51\n")

    def test_eval_returns_correct_predictions_and_RMSE_6(self):
        r = StringIO("2181:\n2410335\n1715714\n2339191\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "2181:\n3.6\n3.8\n3.5\n0.98\n")

    def test_eval_returns_correct_predictions_and_RMSE_7(self):
        r = StringIO("2195:\n968619\n1506412\n201086\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "2195:\n3.9\n3.7\n4.1\n0.81\n")

    def test_eval_returns_correct_predictions_and_RMSE_8(self):
        r = StringIO("3160:\n1983219\n172412\n1719686\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "3160:\n3.5\n3.1\n3.0\n0.80\n")

    def test_eval_returns_correct_predictions_and_RMSE_9(self):
        r = StringIO("3919:\n711755\n1867081\n65058\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "3919:\n3.3\n3.0\n3.1\n0.73\n")


  # def test_eval_returns_correct_predictions_and_RMSE_2(self):
  #       r = StringIO("1:\n30878\n2647871\n1772839\n")
  #       w = StringIO()
  #       netflix_eval(r, w)
  #       self.assertEqual(
  #           w.getvalue(), "1:\n4.9\n3.8\n4.8\n0.52\n")
  
    # def test_getFileName_converts_to_txt_format (self):
    #     w ="2647871" 
    #     result = getFileName(w)
    #     self.assertEqual (result,"mv_2647871.txt")

    # def test_get_rating_given_customer_and_movie(self):
    #     movie = "mv_0002043.txt"
    #     user = 517454
    #     rating = get_rating(user,movie)
    #     self.assertEqual(rating,4)

    # def test_calcCorCoe_get_correlation_coefficient_list_from_users(self):
    #     movie = [3.332, 0, 883478, 4, 4, 4]
    #     user = [4.127, 0, 1772839, 5, 5, 5]
    #     rating = calcCorCoe(user,movie)
    #     self.assertEqual(rating,[3.332, 1.0, 883478, 4, 4, 4])

    # def test_predictRatings_predict_toFind (self):
    #     toFind = [3.691, 0, 1409853, 5, 5, 5]
    #     compare1= [1.809, 0.5892612485683348, 387418, 5, 1, 3, 2]
    #     compare2=  [1.217, 0.4304875602977599, 2439493, 3, 1, 1, 1]
    #     prediction = predictRatings(toFind,compare1,compare2)
    #     self.assertEqual(prediction,3.7097625596868435)

    # def test_findCommonCount_as_toFind(self):
    #     given_user_movie_list = [1452, 3864, 16417, 9208]
    #     max_cust_dict = findCommonCount(given_user_movie_list) 
    #     self.assertEqual(max_cust_dict,{'2439493': 4, '1409853': 4, '305344': 4, '2056022': 4, '387418': 4, '1114324': 4})

    # def test_getAliceList (self):
    #     common_customers = 736115
    #     user_movie_list = [4159, 1452, 11134, 3938]
    #     aList = getAliceList(common_customers,user_movie_list)
    #     self.assertEqual(aList,[3.909, 0, 736115, 1, 5, 5]) 

    # def test_formatForCoco(self):
    #     alice_list = [3.403, 0, 2091526, 3, 3, 5, 4]
    #     maxCustDict = {'2439493': 5, '2091526': 5}
    #     user_movie_list= [15249, 14514, 6736, 10061, 15217]
    #     coco_list = formatForCoco(alice_list,maxCustDict,user_movie_list)
    #     self.assertEqual(coco_list,[[1.217, -0.38605417340643555, 2439493, 1, 1, 1, 1]])

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
